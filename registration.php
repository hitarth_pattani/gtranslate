<?php
/**
 * Copyright © 2015 ZealousWeb. All rights reserved.
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'ZealousWeb_GTranslator',
    __DIR__
);
