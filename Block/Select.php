<?php
/**
 * Copyright © 2016 ZealousWeb. All rights reserved.
 */

namespace ZealousWeb\GTranslator\Block;

use Magento\Framework\View\Element\Template;

/**
 * GTranslator Select
 */ 
class Select extends Template
{    
    /**
     * @return (string) default page language
     */
	public function getPageLanguage()
    {
        $pageLanguage = $this->_scopeConfig->getValue('gtranslator/general/page_language');

        if(!isset($pageLanguage))
        	$pageLanguage = 'en';

        return $pageLanguage;
    }

    /**
     * @return (string) list of comma separated language codes
     */
    public function getLanguagesIncluded()
    {
        $languageIncluded = $this->_scopeConfig->getValue('gtranslator/general/languages_included');

        if(!isset($languageIncluded))
        	$languageIncluded = 'en';

        return $languageIncluded;
    }
}
